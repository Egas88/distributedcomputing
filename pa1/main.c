#include "tools.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>

int main(int argc, char *argv[]) {
    // Проверка аргументов коммандной строки
    int num_processes = 0;
    if (argc < 3) {
        printf("There is not enough arguments for programm, arguments must be: -p X\n");
        exit(1);
    }
    if (strcmp("-p", argv[1]) != 0) {
        printf("argument must be: -p X\n");
        exit(1);
    }
    num_processes = atoi(argv[2]);
    if (num_processes < 1 || num_processes > 10) {
        printf("number of processes must be in [1, 10]\n");
        exit(1);
    }
    FILE* pipe_file_ptr = fopen("pipes.log", "w+");
    FILE* event_file_ptr = fopen("events.log", "w+");
    num_processes = num_processes + 1;

    Channel** channels = init_channels(num_processes, pipe_file_ptr);

    for (local_id i = 1; i < num_processes; i++)
    {
        pid_t pid = fork();
        if (pid < 0) {
            fprintf(stderr, "Error while fork");
            exit(1);
        }
        if (pid == 0) {
            Process child = {.num_process = num_processes, .channels = channels, .pid = i};
            printf("I am child process with id: %d\n", child.pid);

            close_non_related_pipes(&child, pipe_file_ptr);

            send_message(child, STARTED);
            printf(log_started_fmt, i, getpid(), getppid());
            fprintf(event_file_ptr, log_started_fmt, i, getpid(), getppid());

            // Message msg;
            // for (local_id j = 1; j < num_processes; j++)
            // {
            //     if (i != j) {
            //         if (receive(&child, j, &msg) == -1) {
            //             printf("Error while recieve");
            //             exit(-1);
            //         }
            //         printf("%s", msg.s_payload);
            //     }
            // }
            

            if (check_all_recieved(child, STARTED) == 0) {
                printf(log_received_all_started_fmt, i);
                fprintf(event_file_ptr, log_received_all_started_fmt, i);
            } else {
                exit(-1);
            }

            /* 
                Тут должна быть полезная работа
            */

            send_message(child, DONE);
            printf(log_done_fmt, i);
            fprintf(event_file_ptr, log_done_fmt, i);

            if (check_all_recieved(child, DONE) == 0) {
                printf(log_received_all_done_fmt, i);
                fprintf(event_file_ptr, log_received_all_done_fmt, i);
            } else {
                exit(-1);
            }

            close_outcoming_pipes(&child, pipe_file_ptr);
            close_incoming_pipes(&child, pipe_file_ptr);

            exit(0);
        }
    }

    Process parent = {.num_process = num_processes, .channels = channels, .pid = PARENT_ID};
    printf("I am parent prrocess with id: %d\n", parent.pid);

    close_non_related_pipes(&parent, pipe_file_ptr);

    printf(log_started_fmt, PARENT_ID, getpid(), getppid());
    fprintf(event_file_ptr, log_started_fmt, PARENT_ID, getpid(), getppid());

    pid_t waiting_indicator = 0;
    int status = 0;

    if (check_all_recieved(parent, STARTED) == 0) {
        printf(log_received_all_started_fmt, PARENT_ID);
        fprintf(event_file_ptr, log_received_all_started_fmt, PARENT_ID);
    } else {
        exit(-1);
    }

    if (check_all_recieved(parent, DONE) == 0) {
        printf(log_received_all_done_fmt, PARENT_ID);
        fprintf(event_file_ptr, log_received_all_done_fmt, PARENT_ID);
    } else {
        exit(-1);
    }

    close_incoming_pipes(&parent, pipe_file_ptr);
    close_outcoming_pipes(&parent, pipe_file_ptr);

    while((waiting_indicator = wait(&status)) > 0);

    printf(log_done_fmt, PARENT_ID);
    fprintf(event_file_ptr, log_done_fmt, PARENT_ID);

    fclose(pipe_file_ptr);
    fclose(event_file_ptr);
    return 0;
}
