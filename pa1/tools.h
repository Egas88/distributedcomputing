#include "ipc.h"
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include "pa1.h"

#define WRITE 1
#define READ 0

typedef struct {
    int fd[2]; 
} Channel;

typedef struct {
    int num_process;
    Channel** channels;
    local_id pid;
} Process;

Channel** init_channels (int num_process, FILE* log_file_ptr);

void close_outcoming_pipes(Process* proocess, FILE* pipe_file_ptr);

void close_incoming_pipes(Process* proocess, FILE* pipe_file_ptr);

void send_message(Process process, MessageType type);

int check_all_recieved(Process process, MessageType type);

void close_non_related_pipes(Process* process, FILE* pipe_file_ptr);
