#include "tools.h"

int send(void * self, local_id dst, const Message * msg) {
    Process process = *(Process *) self;
    int fd = process.channels[process.pid][dst].fd[WRITE];
    printf("Process %d writing to file descriptor write: %d, read: %d\n",process.pid, fd, process.channels[process.pid][dst].fd[READ]);
    // int write(int fd , void *buf, size_t n);
    if (write(fd, &(msg->s_header), sizeof(MessageHeader)) < 0) {
        fprintf(stderr, "Error while writing from process %d, to process %d\n", process.pid, dst);
        return -1;
    }
    printf("Wtited message len: %d", msg->s_header.s_payload_len);
    return 0;
}

int send_multicast(void * self, const Message * msg) {
    Process process = *(Process *) self;
    for (int i = 0; i < process.num_process; i++)
    {
        if (i != process.pid) {
            if (send(&process, i, msg) == -1) {
                fprintf(stderr, "Error while sending multicast from process %d, at process %d\n", process.pid, i);
                return -1;
            }
        }
    }
    return 0;
}

int receive(void * self, local_id from, Message * msg) {
    Process process = *(Process *) self;
    // size_t read(int fd, void *buf, size_t count);
    int fd =  process.channels[from][process.pid].fd[READ];
    printf("Process %d reading from file descriptor write: %d, read: %d\n",process.pid, process.channels[from][process.pid].fd[WRITE], fd);
    if (read(fd, &(msg->s_header), sizeof(MessageHeader)) == -1) {
        printf("Error while reading message at process %d\n", process.pid);
        return -1;
    }
    printf("Readed message with len %d\n", msg->s_header.s_payload_len);
    return 0;
}

int receive_any(void * self, Message * msg) {
    Process process = *(Process *) self;
    for (int i = 0; i < process.num_process; i++) {
        if (i != process.pid) {
            if (receive(&process, i, msg) == -1) {
                fprintf(stderr, "Error while reading any at process %d, to process %d\n", i, process.pid);
                return -1;
            }
        }
    }
    return 0;
}
