#include "tools.h"


//initialize pipes for Process
Channel** init_channels(int num_process, FILE* log_file_ptr) {
    Channel** arr = (Channel**) malloc(sizeof(Channel*) * num_process);

    for (int i = 0; i < num_process; i++) {
        arr[i] = malloc(sizeof(Channel) * num_process);
    }
    for (int i = 0; i < num_process; i++) {
        for (int j = 0; j < num_process; j++) {
            if (i != j) {
                if (pipe(arr[i][j].fd) == -1) {
                    fprintf(stderr, "Error on creating pipe: write: %d, read: %d\n",
                        arr[i][j].fd[WRITE], arr[i][j].fd[READ]);
                    exit(1);
                }
                fprintf(log_file_ptr, "Created pipe from process %d to process %d, write: %d, read: %d\n", 
                    i, j, arr[i][j].fd[WRITE], arr[i][j].fd[READ]);
            }
        }
    }
    
    return arr;
}

void close_non_related_pipes(Process* process, FILE* pipe_file_ptr) {
    for (int i = 0; i < process->num_process; i++)
    {
        for (int j = 0; j < process->num_process; j++)
        {
            if (i != j && i != process->pid && j != process->pid) {
                close(process->channels[i][j].fd[READ]);
                close(process->channels[i][j].fd[WRITE]);
                fprintf(pipe_file_ptr, "Closed pipe from process %d to process %d, write: %d, read: %d.\n", 
                    i, j, process->channels[process->pid][i].fd[WRITE], process->channels[process->pid][i].fd[READ]);
            } else if (i != j && i == process->pid) {
                close(process->channels[i][j].fd[READ]);
                fprintf(pipe_file_ptr, "Closed read end of pipe from process %d to process %d, read: %d.\n", 
                    i, j, process->channels[i][j].fd[READ]);
                close(process->channels[j][i].fd[WRITE]);
                fprintf(pipe_file_ptr, "Closed write end of pipe from process %d to process %d, write: %d.\n", 
                    j, i, process->channels[j][i].fd[WRITE]);
            } else if (i != j && j == process->pid) {
                close(process->channels[i][j].fd[WRITE]);
                fprintf(pipe_file_ptr, "Closed write end of pipe from process %d to process %d, write: %d.\n", 
                    i, j, process->channels[process->pid][i].fd[WRITE]);
                close(process->channels[j][i].fd[READ]);
                fprintf(pipe_file_ptr, "Closed read end of pipe from process %d to process %d, read: %d.\n", 
                    j, i, process->channels[j][i].fd[READ]);
            }
        }
        
    }
    
}

void close_outcoming_pipes (Process* process, FILE* pipe_file_ptr) {
    for (int i = 0; i < process->num_process; i++)
    {
        if (i != process->pid) {
            close(process->channels[process->pid][i].fd[READ]);
            close(process->channels[process->pid][i].fd[WRITE]);
            fprintf(pipe_file_ptr, "Closed pipe from process %d to process %d, write: %d, read: %d.\n", 
                process->pid, i, process->channels[process->pid][i].fd[WRITE], process->channels[process->pid][i].fd[READ]);
        }
    }
}

void close_incoming_pipes (Process* process, FILE* pipe_file_ptr) {
    for (int i = 0; i < process->num_process; i++)
    {
        if (i != process->pid) {
            close(process->channels[i][process->pid].fd[READ]);
            close(process->channels[i][process->pid].fd[WRITE]);
            fprintf(pipe_file_ptr, "Closed pipe from process %d to process %d, write: %d, read: %d.\n", 
                i, process->pid, process->channels[i][process->pid].fd[WRITE], process->channels[i][process->pid].fd[READ]);
        }
    }
}

void send_message(Process process, MessageType type) {
    MessageHeader header = {.s_local_time = 0, .s_magic = MESSAGE_MAGIC, .s_type = type};
    Message msg = {.s_header = header};
    int length;
    if (type == STARTED) {
        length = sprintf(msg.s_payload, log_started_fmt, process.pid, getpid(), getppid());
    } else {
        length = sprintf(msg.s_payload, log_done_fmt, process.pid);
    }
    msg.s_header.s_payload_len = length;
    if (send_multicast(&process, &msg) == -1) {
        printf("Error while multicasting at process %d", process.pid);
        exit(-1);
    }
}

int check_all_recieved(Process process, MessageType type) {
    int count = 0;
    for (int i = 1; i < process.num_process; i++)
    {
        if (i != process.pid) {
            Message msg;
            if (receive(&process, i, &msg) == -1) {
                printf("Error while recieving messages\n");
                return -1;
            }
            if (msg.s_header.s_type == type) {
                count++;
                printf("Process %d readed %d messages with type %s\n", 
                    process.pid, count, type == 0 ? "STARTED" : "DONE");
            }
        }
    }
    if (process.pid != 0 && count == process.num_process-2) { // -2 Потому что родитель не отправляет сообщений и не нужно принимать сообщения от себя
        return 0;
    } else if (process.pid == 0 && count == process.num_process - 1) { // -1 Потому что родитель не отправляет сообщений
        return 0;
    }
    return -1;
}
