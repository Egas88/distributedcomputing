#include "ipc.h"
#include "ipc.h"
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>  /* Definition of O_* constants */
#include "banking.h"
#include "pa2345.h"

#define WRITE 1
#define READ 0

typedef struct {
    int fd[2]; 
} Channel;

typedef struct {
    int num_process;
    Channel** channels;
    local_id pid;
    timestamp_t last_time;
    balance_t balance;
    BalanceHistory history;
} Process;

Channel** init_channels (int num_process, FILE* log_file_ptr);

// void add_nb_flag(Channel** channels, int num_processes);

void close_outcoming_pipes(Process* process, FILE* pipe_file_ptr);

void close_incoming_pipes(Process* process, FILE* pipe_file_ptr);

void send_message(Process* process, MessageType type, TransferOrder* order);

int check_all_recieved(Process* process, MessageType type);

void close_non_related_pipes(Process* process, FILE* pipe_file_ptr);

void bank_operations(Process *process, FILE* event_file_ptr);

void receive_histories(Process process);

timestamp_t add_to_history(BalanceHistory* history, timestamp_t last_time, timestamp_t time, balance_t balance, balance_t amount);
