#include "tools.h"
#include <errno.h>

int send(void * self, local_id dst, const Message * msg) {
    Process process = *(Process *) self;
    int fd = process.channels[process.pid][dst].fd[WRITE];
    printf("Process %d writing to file descriptor write: %d, read: %d\n",process.pid, fd, process.channels[process.pid][dst].fd[READ]);
    // int write(int fd , void *buf, size_t n);
    if (write(fd, &(msg->s_header), sizeof(MessageHeader) + msg->s_header.s_payload_len) < 0) {
        fprintf(stderr, "Error while writing from process %d, to process %d\n", process.pid, dst);
        return -1;
    }
    printf("Wtited message len: %d\n", msg->s_header.s_payload_len);
    return 0;
}

int send_multicast(void * self, const Message * msg) {
    Process process = *(Process *) self;
    for (int i = 0; i < process.num_process; i++)
    {
        if (i != process.pid) {
            if (send(&process, i, msg) == -1) {
                fprintf(stderr, "Error while sending multicast from process %d, at process %d\n", process.pid, i);
                return -1;
            }
        }
    }
    return 0;
}


int is_data_avialable(int read_fd, Message * msg) {
    const int status = read(read_fd, &(msg->s_header), sizeof(MessageHeader)); 

    if ((status == -1 && errno == EAGAIN) || status == 0) {
        return 2;
    }
    if (status < 0) {
        return 1;
    }
    return 0;
}


int read_message_body(int read_fd, Message * msg) {

    if (msg->s_header.s_payload_len == 0) {
        printf("Readed message with len %d\n", msg->s_header.s_payload_len);
        return 0;
    }

    while (1) {
        int status = read(read_fd, &(msg->s_payload), msg->s_header.s_payload_len);
        if ((status == -1 && errno == EAGAIN) || status == 0) {
            continue;
        }
        if (status == msg->s_header.s_payload_len) {
            break;
        } else {
            printf("Error on read payload\n");
            return 1;
        }
    }

    printf("Readed message with len %d\n", msg->s_header.s_payload_len);
    return 0;
}


int receive(void * self, local_id from, Message * msg) {
    Process process = *(Process *) self;
    // size_t read(int fd, void *buf, size_t count);
    int fd =  process.channels[from][process.pid].fd[READ];
    printf("Process %d reading from file descriptor write: %d, read: %d\n",process.pid, process.channels[from][process.pid].fd[WRITE], fd);

    while (1) {
        int status = is_data_avialable(fd, msg);
        if (status == 2) {
            continue;
        }
        if (status == 0) {
            break;
        } else {
            printf("Error on read header\n");
            return 1;
        }
    }
    return read_message_body(fd, msg);
}

int receive_any(void * self, Message * msg) {
    Process process = *(Process *) self;
    while (1)
    {
        for (local_id i = 0; i < process.num_process; i++) {
            if (i != process.pid) {
                int fd = process.channels[i][process.pid].fd[READ];
                int status = is_data_avialable(fd, msg);
                if (status == 2) {
                    continue;
                }
                
                if (status == 1) {
                    return 1;
                }

                return read_message_body(fd, msg);
            }
        }
    }
    return 0;
}
