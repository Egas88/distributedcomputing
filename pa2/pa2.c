#include "tools.h"
#include <sys/types.h>
#include <sys/wait.h>

void transfer(void * parent_data, local_id src, local_id dst,
              balance_t amount)
{
    // student, please implement me
    TransferOrder order = {.s_src = src, .s_dst = dst, .s_amount = amount};
    send_message(parent_data, TRANSFER, &order);
    Message msg;

    if (receive(parent_data, dst, &msg) == -1) {
        printf("Error on receiving ACK");
        exit(1);
    }
}

int main(int argc, char *argv[]) {

    // ARGS

    int num_processes = 0;
    if (argc < 3) {
        printf("There is not enough arguments for programm, arguments must be: -p X\n");
        exit(1);
    }
    if (strcmp("-p", argv[1]) != 0) {
        printf("argument must be: -p X\n");
        exit(1);
    }
    num_processes = atoi(argv[2]);
    if (num_processes < 1 || num_processes > 10) {
        printf("number of processes must be in [1, 10]\n");
        exit(1);
    }

    num_processes = num_processes + 1;


    //FILES

    FILE* pipe_file_ptr = fopen("pipes.log", "w+");
    FILE* event_file_ptr = fopen("events.log", "w+");


    //BALANCE

    if (argc < num_processes + 2) {
        printf("There is not enough arguments for programm, arguments must be: -p X then X numbers each between [1;99]\n");
        exit(1);
    }
    if (argc > num_processes + 2) {
        printf("There is too much arguments for programm, arguments must be: -p X then X numbers each between [1;99]\n");
        exit(1);
    }

    int S[num_processes-1];

    for (int i = 3; i < 2 + num_processes; i++)
    {
        int amount = atoi(argv[i]);
        if (amount < 1 || amount > 99) {
            printf("wrong number for argument at position %d, argument must be in [1;99]\n", i+1);
            exit(1);
        }
        S[i-3] = amount;
    }

    //INIT PROCESSES

    Channel** channels = init_channels(num_processes, pipe_file_ptr);
    // add_nb_flag(channels, num_processes);

    for (local_id i = 1; i < num_processes; i++)
    {
        pid_t pid = fork();
        if (pid < 0) {
            fprintf(stderr, "Error while fork");
            exit(1);
        }
        if (pid == 0) {
            Process child = {.num_process = num_processes, .channels = channels, .pid = i, .balance = S[i-1],
             .history = {.s_id = i, .s_history_len = 0}, .last_time = get_physical_time()};
            printf("I am child process with id: %d\n", child.pid);

            close_non_related_pipes(&child, pipe_file_ptr);

            //STARTED

            send_message(&child, STARTED, NULL);
            printf(log_started_fmt, get_physical_time(), i, getpid(), getppid(), child.balance);
            fprintf(event_file_ptr, log_started_fmt, get_physical_time(), i, getpid(), getppid(), child.balance);
            

            if (check_all_recieved(&child, STARTED) == 0) {
                printf(log_received_all_started_fmt, get_physical_time(), i);
                fprintf(event_file_ptr, log_received_all_started_fmt, get_physical_time(), i);
            } else {
                exit(-1);
            }

            //WORK

            bank_operations(&child, event_file_ptr);

            //DONE

            // send_message(&child, DONE, NULL);

            // printf(log_done_fmt, get_physical_time(), i, child.balance);
            // fprintf(event_file_ptr, log_done_fmt, get_physical_time(), i, child.balance);

            // if (check_all_recieved(&child, DONE) == 0) {
            //     printf(log_received_all_done_fmt, get_physical_time(), i);
            //     fprintf(event_file_ptr, log_received_all_done_fmt, get_physical_time(), i);
            // } else {
            //     exit(-1);
            // }

            close_outcoming_pipes(&child, pipe_file_ptr);
            close_incoming_pipes(&child, pipe_file_ptr);

            exit(0);
        }
    }

    Process parent = {.num_process = num_processes, .channels = channels, .pid = PARENT_ID};
    printf("I am parent prrocess with id: %d\n", parent.pid);

    close_non_related_pipes(&parent, pipe_file_ptr);

    pid_t waiting_indicator = 0;
    int status = 0;

    //STARTED

    if (check_all_recieved(&parent, STARTED) == 0) {
        printf(log_received_all_started_fmt, get_physical_time(), PARENT_ID);
        fprintf(event_file_ptr, log_received_all_started_fmt, get_physical_time(), PARENT_ID);
    } else {
        exit(-1);
    }

    //WORK
    bank_robbery(&parent, num_processes-1);

    //SEND STOP
    send_message(&parent, STOP, NULL);

    //DONE

    if (check_all_recieved(&parent, DONE) == 0) {
        printf(log_received_all_done_fmt, get_physical_time(), PARENT_ID);
        fprintf(event_file_ptr, log_received_all_done_fmt, get_physical_time(), PARENT_ID);
    } else {
        exit(-1);
    }

    receive_histories(parent);

    close_incoming_pipes(&parent, pipe_file_ptr);
    close_outcoming_pipes(&parent, pipe_file_ptr);

    while((waiting_indicator = wait(&status)) > 0);

    fclose(pipe_file_ptr);
    fclose(event_file_ptr);
    return 0;
}
