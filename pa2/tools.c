#include "tools.h"


//initialize pipes for Process
Channel** init_channels(int num_process, FILE* log_file_ptr) {
    Channel** arr = (Channel**) malloc(sizeof(Channel*) * num_process);

    for (int i = 0; i < num_process; i++) {
        arr[i] = malloc(sizeof(Channel) * num_process);
    }

    for (int i = 0; i < num_process; i++) {
        for (int j = 0; j < num_process; j++) {
            if (i != j) {
                if (pipe(arr[i][j].fd) == -1) {
                    fprintf(stderr, "Error on creating pipe: write: %d, read: %d\n",
                        arr[i][j].fd[WRITE], arr[i][j].fd[READ]);
                    exit(1);
                }

                //  Добавление флагов Nonblock, так как pipe2 не работает

                if (fcntl(arr[i][j].fd[0], F_SETFL, O_NONBLOCK) != 0) {
                    printf("АШИБОЧКА\n");
                }

                if (fcntl(arr[i][j].fd[1], F_SETFL, O_NONBLOCK) != 0) {
                    printf("АШИБОЧКА\n");
                }

                fprintf(log_file_ptr, "Created pipe from process %d to process %d, write: %d, read: %d\n", 
                    i, j, arr[i][j].fd[WRITE], arr[i][j].fd[READ]);
            }
        }
    }
    
    return arr;
}

void close_non_related_pipes(Process* process, FILE* pipe_file_ptr) {
    for (int i = 0; i < process->num_process; i++)
    {
        for (int j = 0; j < process->num_process; j++)
        {
            if (i != j && i != process->pid && j != process->pid) {

                close(process->channels[i][j].fd[READ]);
                close(process->channels[i][j].fd[WRITE]);
                fprintf(pipe_file_ptr, "Closed pipe from process %d to process %d, write: %d, read: %d.\n", 
                    i, j, process->channels[process->pid][i].fd[WRITE], process->channels[process->pid][i].fd[READ]);

            } else if (i != j && i == process->pid) {

                close(process->channels[i][j].fd[READ]);
                fprintf(pipe_file_ptr, "Closed read end of pipe from process %d to process %d, read: %d.\n", 
                    i, j, process->channels[i][j].fd[READ]);

                close(process->channels[j][i].fd[WRITE]);
                fprintf(pipe_file_ptr, "Closed write end of pipe from process %d to process %d, write: %d.\n", 
                    j, i, process->channels[j][i].fd[WRITE]);
            }
        }
        
    }
}

// void add_nb_flag(Channel** channels, int num_processes) {
//     for (int i = 0; i < num_processes; i++)
//     {
//         for (int j = 0; j < num_processes; j++)
//         {
//             int flags = fcntl(channels[i][j].fd[0], F_GETFL);
//             if (fcntl(channels[i][j].fd[0], F_SETFL, flags | O_NONBLOCK) != 0) {
//                     printf("АШИБОЧКА\n");
//             }
            
//             flags = fcntl(channels[i][j].fd[1], F_GETFL);
//             if (fcntl(channels[i][j].fd[1], F_SETFL, flags | O_NONBLOCK) != 0) {
//                 printf("АШИБОЧКА\n");
//             }
//         }
        
//     }
    
// }


void close_outcoming_pipes (Process* process, FILE* pipe_file_ptr) {
    for (int i = 0; i < process->num_process; i++)
    {
        if (i != process->pid) {
            close(process->channels[process->pid][i].fd[READ]);
            close(process->channels[process->pid][i].fd[WRITE]);
            fprintf(pipe_file_ptr, "Closed pipe from process %d to process %d, write: %d, read: %d.\n", 
                process->pid, i, process->channels[process->pid][i].fd[WRITE], process->channels[process->pid][i].fd[READ]);
        }
    }
}

void close_incoming_pipes (Process* process, FILE* pipe_file_ptr) {
    for (int i = 0; i < process->num_process; i++)
    {
        if (i != process->pid) {
            close(process->channels[i][process->pid].fd[READ]);
            close(process->channels[i][process->pid].fd[WRITE]);
            fprintf(pipe_file_ptr, "Closed pipe from process %d to process %d, write: %d, read: %d.\n", 
                i, process->pid, process->channels[i][process->pid].fd[WRITE], process->channels[i][process->pid].fd[READ]);
        }
    }
}


void receive_histories(Process process) {
    AllHistory all_history = {.s_history_len = process.num_process-1};
    for (local_id i = 1; i < process.num_process; i++)
    {
        Message msg;
        if (receive(&process, i, &msg) != 0) {
            printf("Andrew we've got corpse, maybe criminal\n");
            exit(1);
        }
        BalanceHistory history;
        memcpy(&history, msg.s_payload, msg.s_header.s_payload_len);
        all_history.s_history[i-1] = history;
    }
    print_history(&all_history);
}


void send_message(Process* process, MessageType type, TransferOrder* order) {
    timestamp_t time = get_physical_time();
    MessageHeader header = {.s_local_time = time, .s_magic = MESSAGE_MAGIC, .s_type = type};
    Message msg = {.s_header = header};
    int length = 0;
    switch (type)
    {
    case STARTED:
        length = sprintf(msg.s_payload, log_started_fmt, time, process->pid, getpid(), getppid(), process->balance);
        msg.s_header.s_payload_len = length;
        if (send_multicast(process, &msg) == -1) {
            printf("Error while multicasting at process %d", process->pid);
            exit(1);
        }
        break;
    case DONE:
        length = sprintf(msg.s_payload, log_done_fmt, time, process->pid, process->balance);
        msg.s_header.s_payload_len = length;
        if (send_multicast(process, &msg) == -1) {
            printf("Error while multicasting at process %d", process->pid);
            exit(1);
        }
        break;
    case TRANSFER:
        msg.s_header.s_payload_len = sizeof(TransferOrder);
        memcpy(msg.s_payload, (char*) order, sizeof(TransferOrder));
        if (send(process, order->s_src, &msg) == -1) {
            printf("Error while sending transfer message\n");
            exit(1);
        }
        break;
    case STOP:
        msg.s_header.s_payload_len = 0;
        if (send_multicast(process, &msg)) {
            printf("Error while multicasting at process %d", process->pid);
            exit(1);
        }
        break;
    case ACK:
        msg.s_header.s_payload_len = 0;
        if (send(process, 0, &msg)) { // Всегда отправляется родителю
            printf("Error while sending ACK message");
            exit(1);
        }
        break;
    case BALANCE_HISTORY:
        length = sizeof(process->history.s_id) + sizeof(process->history.s_history_len) + sizeof(BalanceState) * process->history.s_history_len;
        msg.s_header.s_payload_len = length;
        memcpy(msg.s_payload, &(process->history), length);
        if (send(process, 0, &msg)) {
            printf("Error while sending BH message");
            exit(1);
        }
        break;
    default:
        break;
    }
}

void bank_operations(Process *process, FILE* event_file_ptr) { // Сюда пришлось добавить проверку и на DONE
    int count_done = 0;
    int is_stopped = 0;
    while(1) {
        if (is_stopped && (count_done == process->num_process - 2)) {
            printf(log_received_all_done_fmt, get_physical_time(), process->pid);
            fprintf(event_file_ptr, log_received_all_done_fmt, get_physical_time(), process->pid);
            timestamp_t time = get_physical_time();
            process->last_time = add_to_history(&(process->history), process->last_time, time, process->balance, 0); // Обновление истории
            send_message(process, BALANCE_HISTORY, NULL);
            return ;
        }
        Message msg;
        TransferOrder order;
        if (receive_any(process, &msg) == -1) {
            printf("Error whule recieving any at bank operations\n");
            exit(1);
        }
        printf("%d\n", msg.s_header.s_type);
        switch (msg.s_header.s_type)
        {
        case TRANSFER:
            order = *(TransferOrder *) msg.s_payload;
            printf("Order src number is %d WHILE PROCESS PID is %d\n", order.s_src, process->pid);

            if (order.s_src == process->pid) {
                msg.s_header.s_local_time = get_physical_time();
                timestamp_t time = msg.s_header.s_local_time;
                process->last_time = add_to_history(&(process->history), process->last_time, time, process->balance, -order.s_amount);
                process->balance -= order.s_amount;
                fprintf(event_file_ptr, log_transfer_out_fmt, time, order.s_src, order.s_amount, order.s_dst);
                printf(log_transfer_out_fmt, time, order.s_src, order.s_amount, order.s_dst);
                if (send(process, order.s_dst, &msg) == -1) {
                    printf("Error on second transfer at process %d\n", process->pid);
                    return;
                }
            } else {
                msg.s_header.s_local_time = get_physical_time();
                int time = msg.s_header.s_local_time;
                process->last_time = add_to_history(&(process->history), process->last_time, time, process->balance, order.s_amount);
                process->balance += order.s_amount;
                fprintf(event_file_ptr, log_transfer_in_fmt, time, order.s_dst, order.s_amount, order.s_src);
                printf(log_transfer_in_fmt, time, order.s_dst, order.s_amount, order.s_src);
                send_message(process, ACK, NULL);
            }
            break;
        case STOP:
            is_stopped++;
            if (is_stopped > 1) {
                printf("АШИБАЧКА!!!!!\n");
                exit(1);
            }
            send_message(process, DONE, NULL);
            printf(log_done_fmt, get_physical_time(), process->pid, process->balance);
            fprintf(event_file_ptr, log_done_fmt, get_physical_time(), process->pid, process->balance);
            break;
        case DONE:
            count_done++;
            break;
        default:
            break;
        }
    }
}


timestamp_t add_to_history(BalanceHistory* history, timestamp_t last_time, timestamp_t time, balance_t balance, balance_t amount) {
    for (timestamp_t i = last_time; i < time; i++)
    {
        history->s_history[i] = (BalanceState) {.s_balance = balance, .s_balance_pending_in = 0, .s_time = i};
    }
    history->s_history[time] = (BalanceState) {.s_balance = balance + amount, .s_balance_pending_in = 0, .s_time = time};
    history->s_history_len = time + 1;
    return time;
}


int check_all_recieved(Process* process, MessageType type) {
    int count = 0;
    for (int i = 1; i < process->num_process; i++)
    {
        if (i != process->pid) {
            Message msg;
            if (receive(process, i, &msg) == -1) {
                printf("Error while recieving messages\n");
                return -1;
            }
            if (msg.s_header.s_type == type) {
                count++;
                printf("Process %d readed %d messages with type %s\n", 
                    process->pid, count, type == 0 ? "STARTED" : "DONE");
            }
        }
    }
    if (process->pid != 0 && count == process->num_process-2) { // -2 Потому что родитель не отправляет сообщений и не нужно принимать сообщения от себя
        return 0;
    } else if (process->pid == 0 && count == process->num_process - 1) { // -1 Потому что родитель не отправляет сообщений
        return 0;
    }
    return -1;
}
